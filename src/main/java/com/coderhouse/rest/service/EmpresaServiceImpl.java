package com.coderhouse.rest.service;

import com.coderhouse.rest.entity.Cliente;
import com.coderhouse.rest.entity.Empresa;
import com.coderhouse.rest.repository.EmpresaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class EmpresaServiceImpl implements EmpresaService{

    @Autowired
    EmpresaRepository empresaRepository;

    @Override
    public Empresa getEmpresa(Long id) {
        Empresa empresaObtener = empresaRepository.findById(id).orElseThrow(RuntimeException::new);
        return empresaObtener;
    }

    @Override
    public List<Empresa> traerTodasEmpresas() {
        return empresaRepository.findAll();
    }

    @Override
    public Empresa nuevaEmpresa(Empresa empresa) {
        return empresaRepository.save(empresa);
    }

    @Override
    public Empresa modificarEmpresa(Empresa empresa) {
        Empresa empresaModificado = empresaRepository.findById(empresa.getId()).get();
        empresaModificado.setNombre(empresa.getNombre());
        empresaModificado.setRubro(empresa.getRubro());
        return empresaRepository.save(empresa);
    }


    @Override
    public String borrarEmpresa(Long id) {
        Empresa empresa = empresaRepository.findById(id).get();
        empresaRepository.deleteById(id);
        log.info("Empresa borrada del sistema "+empresa.getNombre());
        return "Se borro una empresa llamada "+empresa.getNombre();
    }
}

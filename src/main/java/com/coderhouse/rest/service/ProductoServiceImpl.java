package com.coderhouse.rest.service;

import com.coderhouse.rest.entity.Cliente;
import com.coderhouse.rest.entity.Producto;
import com.coderhouse.rest.repository.ProductoRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class ProductoServiceImpl implements ProductoService{

    @Autowired
    ProductoRepository productoRepository;

    @Override
    public Producto getProducto(Long id) {
        Producto productoObtener = productoRepository.findById(id).orElseThrow(RuntimeException::new);
        return productoObtener;
    }

    @Override
    public List<Producto> traerTodosProductos() {
        return productoRepository.findAll();
    }

    @Override
    public Producto nuevoProducto(Producto producto) {
        return productoRepository.save(producto);
    }

    @Override
    public Producto modificarProducto(Producto producto) {
        Producto modificaProducto = productoRepository.findById(producto.getId()).get();
        modificaProducto.setNombre(producto.getNombre());
        modificaProducto.setPrecio(producto.getPrecio());
        modificaProducto.setStock(producto.getStock());
        return productoRepository.save(producto);
    }

    @Override
    public String borrarProducto(Long id) {
        Producto producto = productoRepository.findById(id).get();
        productoRepository.deleteById(id);
        log.info("Producto borrado del sistema "+producto.getNombre());
        return "Borraste un producto: "+producto.getNombre();
    }
}

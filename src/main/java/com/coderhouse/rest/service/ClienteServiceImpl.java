package com.coderhouse.rest.service;

import com.coderhouse.rest.dto.ClienteDto;
import com.coderhouse.rest.entity.Cliente;
import com.coderhouse.rest.repository.ClienteRepository;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
@Slf4j
public class ClienteServiceImpl implements ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    public Cliente getCliente(Long id){
        Cliente clienteObtener = clienteRepository.findById(id).orElseThrow(RuntimeException::new);
        return clienteObtener;
    }

    public List<Cliente> traerTodosClientes(){

  /*      List<Cliente> edadClientes = clienteRepository.findAll();
        edadClientes.forEach(cliente -> {
            if (cliente.getNombre().equals("Patricia")){
                cliente.setNombre("Juanita");
            }
        }); */
        return clienteRepository.findAll();
    }

    public ClienteDto getClienteDto(Long id){
        Cliente clienteObtener = clienteRepository.findById(id).orElseThrow(RuntimeException::new);
        ClienteDto clienteDto = new ClienteDto();
        clienteDto.setNombre(clienteObtener.getNombre());
        clienteDto.setApellido(clienteObtener.getApellido());
        clienteDto.setDni(clienteObtener.getDni());
        clienteDto.setTelefono(clienteObtener.getTelefono());
        clienteDto.setEmail(clienteObtener.getEmail());
        return clienteDto;
    }

    public Cliente nuevoCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente modificarCliente(Cliente cliente) {
        Cliente modificaCliente = clienteRepository.findById(cliente.getId()).get();
        modificaCliente.setNombre(cliente.getNombre());
        modificaCliente.setApellido(cliente.getApellido());
        modificaCliente.setDni(cliente.getDni());
        return clienteRepository.save(cliente);
    }

    public String borrarCliente(Long id){
        Cliente cliente = clienteRepository.findById(id).get();
        clienteRepository.deleteById(id);
        log.info("Cliente borrado del sistema "+cliente.getNombre()+" "+cliente.getApellido());
        return "Borraste a "+cliente.getNombre()+" "+cliente.getApellido();
    }

}

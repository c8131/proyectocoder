package com.coderhouse.rest.service;

import com.coderhouse.rest.entity.Producto;

import java.util.List;

public interface ProductoService {
    public Producto getProducto(Long id);

    public List<Producto> traerTodosProductos();

    public Producto nuevoProducto(Producto producto);

    public  Producto modificarProducto(Producto producto);

    public String borrarProducto(Long id);
}

package com.coderhouse.rest.service;

import com.coderhouse.rest.dto.ClienteDto;
import com.coderhouse.rest.entity.Cliente;

import java.util.List;

public interface ClienteService {

    public Cliente getCliente(Long id);

    public List<Cliente> traerTodosClientes();

    //public List<Cliente> calularEdades();

    //public Cliente calculaEdadCliente(Long id);

    public ClienteDto getClienteDto(Long id);

    public Cliente nuevoCliente(Cliente cliente);

    public  Cliente modificarCliente(Cliente cliente);

    public String borrarCliente(Long id);
}

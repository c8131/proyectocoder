package com.coderhouse.rest.service;

import com.coderhouse.rest.entity.Empresa;

import java.util.List;

public interface EmpresaService {

    public Empresa getEmpresa(Long id);

    public List<Empresa> traerTodasEmpresas();

    public Empresa nuevaEmpresa(Empresa empresa);

    public  Empresa modificarEmpresa(Empresa empresa);

    public String borrarEmpresa(Long id);
}

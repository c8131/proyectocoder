package com.coderhouse.rest.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "empresas")
@Data
@NoArgsConstructor
public class Empresa {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "rubro")
    private String rubro;

    @OneToMany(mappedBy = "empresa")
    private List<Factura> factura;
}

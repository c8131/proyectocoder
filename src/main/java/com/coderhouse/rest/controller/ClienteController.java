package com.coderhouse.rest.controller;

import com.coderhouse.rest.dto.ClienteDto;
import com.coderhouse.rest.entity.Cliente;
import com.coderhouse.rest.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClienteController {
    @Autowired
    ClienteService clienteService;

    @GetMapping("/getCliente/{id}")
    public Cliente getCliente(@PathVariable(value = "id") Long id){
        return clienteService.getCliente(id);
    }

    @GetMapping("/traerTodosClientes")
    public List<Cliente> getClientes(){
        return clienteService.traerTodosClientes();
    }

    @GetMapping("/getClienteDto/{id}")
    public ClienteDto getClienteDto(@PathVariable(value = "id") Long id){
        return clienteService.getClienteDto(id);
    }

    @PostMapping("/postCliente")
    public ResponseEntity<Cliente> getCliente(@RequestBody Cliente cliente, BindingResult result) {
        Cliente nuevoCliente = clienteService.nuevoCliente(cliente);
        if (result.hasErrors()){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(nuevoCliente);
    }

/*    public Cliente postCliente(@RequestBody Cliente cliente, BindingResult resul){
        if (resul.hasErrors()){}
        return clienteService.nuevoCliente(cliente);

    } */

    @PutMapping("/editarCliente")
    public Cliente modificarCliente(@RequestBody Cliente cliente){
        return clienteService.modificarCliente(cliente);
    }

    @DeleteMapping("/borrarCliente/{id}")
    public String borrarCliente(@PathVariable("id") Long id){
        return clienteService.borrarCliente(id);
    }
}

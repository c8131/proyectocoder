package com.coderhouse.rest.controller;

import com.coderhouse.rest.entity.Cliente;
import com.coderhouse.rest.entity.Empresa;
import com.coderhouse.rest.service.EmpresaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmpresaController {
    @Autowired
    EmpresaService empresaService;

    @GetMapping("/getEmpresa/{id}")
    public Empresa getEmpresa(@PathVariable(value = "id") Long id){

        return empresaService.getEmpresa(id);
    }

    @GetMapping("/traerTodasEmpresas")
    public List<Empresa> getEmpresas(){

        return empresaService.traerTodasEmpresas();
    }

    @PostMapping("/postEmpresa")
    public Empresa postEmpresa(@RequestBody Empresa empresa){
      return empresaService.nuevaEmpresa(empresa);
    }

    @DeleteMapping("/borrarEmpresa/{id}")
    public String borrarEmpresa(@PathVariable("id") Long id){
        return empresaService.borrarEmpresa(id);
    }

    @PutMapping("/editarEmpresa")
    public Empresa modificarEmpresa(@RequestBody Empresa empresa){
        return empresaService.modificarEmpresa(empresa);
    }
}

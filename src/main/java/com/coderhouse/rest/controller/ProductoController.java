package com.coderhouse.rest.controller;

import com.coderhouse.rest.entity.Cliente;
import com.coderhouse.rest.entity.Producto;
import com.coderhouse.rest.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/getProducto/{id}")
    public Producto getProducto(@PathVariable(value = "id") Long id){
        return productoService.getProducto(id);
    }

    @GetMapping("/traerTodosProductos")
    public List<Producto> getProductos(){

        return productoService.traerTodosProductos();
    }

    @PostMapping("/postProducto")
    public Producto postProducto(@RequestBody Producto producto){
        return productoService.nuevoProducto(producto);

    }

    @PutMapping("/editarProducto")
    public Producto modificarProducto(@RequestBody Producto producto){
        return productoService.modificarProducto(producto);
    }

    @DeleteMapping("/borrarProducto/{id}")
    public String borrarProducto(@PathVariable("id") Long id){
        return productoService.borrarProducto(id);
    }


}

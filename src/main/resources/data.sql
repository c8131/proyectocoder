

CREATE TABLE productos (
	id BIGINT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(150) NOT NULL ,
	precio DOUBLE NOT NULL ,
	stock INT NOT NULL ,
	PRIMARY KEY (id));


CREATE TABLE clientes (
	id BIGINT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(150) NOT NULL ,
	apellido VARCHAR(150) NOT NULL ,
	dni VARCHAR(250) NOT NULL ,
	telefono VARCHAR(11) NULL DEFAULT NULL ,
	email VARCHAR(150) NULL DEFAULT NULL , 	PRIMARY KEY (id));


CREATE TABLE empresas (
	id BIGINT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(150) NOT NULL ,
	rubro VARCHAR(150), 	PRIMARY KEY (id));

CREATE TABLE facturas (
	id BIGINT NOT NULL AUTO_INCREMENT,
	id_cliente BIGINT NOT NULL ,
	id_empresa BIGINT NOT NULL ,
	fecha DATE NOT NULL,
	monto_total DOUBLE NOT NULL, PRIMARY KEY (id));

ALTER TABLE facturas
ADD FOREIGN KEY (id_cliente) REFERENCES clientes(id);
ALTER TABLE facturas
ADD FOREIGN KEY (id_empresa) REFERENCES empresas(id);

CREATE TABLE detalles (
	id BIGINT NOT NULL AUTO_INCREMENT,
	id_factura BIGINT NOT NULL ,
	id_producto BIGINT NOT NULL ,
	cantidad INT NOT NULL, PRIMARY KEY (id));

ALTER TABLE detalles
ADD FOREIGN KEY (id_factura) REFERENCES facturas(id);
ALTER TABLE detalles
ADD FOREIGN KEY (id_producto) REFERENCES productos(id);

INSERT INTO PRODUCTOS (id, nombre, precio, stock) VALUES (NULL, 'Televisor', 1000, 100);
INSERT INTO PRODUCTOS (id, nombre, precio, stock) VALUES (NULL, 'Pendrive', 100, 100);
INSERT INTO PRODUCTOS (id, nombre, precio, stock) VALUES (NULL, 'Leche con chocolate', 10, 50);
INSERT INTO PRODUCTOS (id, nombre, precio, stock) VALUES (NULL, 'Zapatillas Nike', 300, 20);
INSERT INTO PRODUCTOS (id, nombre, precio, stock) VALUES (NULL, 'Iroman marvel', 150, 30);

INSERT INTO CLIENTES (id, nombre, apellido, dni, telefono, email) VALUES (NULL, 'Juan', 'Pérez', '23435435', '56974613333', 'juanperez@gmail.com');
INSERT INTO CLIENTES (id, nombre, apellido, dni, telefono, email) VALUES (NULL, 'Andres', 'Silva', '64565645', '5679257833', 'andres@gmail.com');
INSERT INTO CLIENTES (id, nombre, apellido, dni, telefono, email) VALUES (NULL, 'Maria', 'De las Nieves', '423453457', '22345672', 'maria@gmail.com');
INSERT INTO CLIENTES (id, nombre, apellido, dni, telefono, email) VALUES (NULL, 'Paula', 'Fuentes', '78574746', '234423454', 'paula@gmail.com');
INSERT INTO CLIENTES (id, nombre, apellido, dni, telefono, email) VALUES (NULL, 'Fabiola', 'Ramírez', '123343543', '43534543', '');

INSERT INTO EMPRESAS (id, nombre, rubro) VALUES (NULL, 'FALABELLA', 'Multitienda');
INSERT INTO EMPRESAS (id, nombre, rubro) VALUES (NULL, 'WALMART', 'Supermercado');

